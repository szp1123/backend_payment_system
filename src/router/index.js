import Vue from 'vue';
import VueRouter from 'vue-router'
import RouterConfig from './router.config';

Vue.use(VueRouter);

export default new VueRouter(RouterConfig);

