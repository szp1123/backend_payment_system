
import Login from '../components/Login/Login';
import Index from '../components/Index/Index';
import Pay from '../components/Pay/Pay';
import PayRoll from '../components/PayRoll/PayRoll';
import PayUser from '@/components/User/PayUser';
import PayrollUser from '@/components/User/PayrollUser';
import PayMer from '@/components/Mer/PayMer';
import PayrollMer from '@/components/Mer/PayrollMer';
import N404 from '@/components/404/404';
export default {
  routes:[
    {
      path:'/',
      redirect:'/login'
    },
    // 登录
    {
      path:'/login',
      name:'login',
      component:Login,
      meta: {
        title: '登录'
      }
    },
    // 主页
    {
      path:'/index',
      name: 'index',
      component: Index,
      children: [
        // 支付
        // 订单信息
        {
          path: '/index/pay',
          name: 'pay',
          component: Pay,
          meta: {
            title: '订单信息-支付'
          }
        },
        // 商户信息
        {
          path: '/index/payuser',
          name: 'payuser',
          component: PayUser,
          meta: {
            title: '商户信息-支付'
          }
        },
        // 渠道信息
        {
          path: '/index/paymer',
          name: 'paymer',
          component: PayMer,
          meta: {
            title: '渠道信息-支付'
          }
        },
        // 代付
        // 订单信息
        {
          path: '/index/payroll',
          name: 'payroll',
          component: PayRoll,
          meta: {
            title: '订单信息-代付'
          }
        },
        // 商户信息
        {
          path: '/index/payrolluser',
          name: 'payrolluser',
          component: PayrollUser,
          meta: {
            title: '商户信息-代付'
          }
        },
        // 渠道信息
        {
          path: '/index/payrollmer',
          name: 'payrollmer',
          component: PayrollMer,
          meta: {
            title: '渠道信息-代付'
          }
        },
        {
          path: '/index',
          redirect: '/index/pay',
        }
      ]
    },
    {
      path: '/404',
      component: N404,
      meta: {
        title: '404'
      }
    },
    {
      path: '*',
      redirect: '/404'
    }
  ],
  linkActiveClass:'active',
  base: 'public/backstage'
}
