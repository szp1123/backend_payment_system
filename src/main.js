// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.prototype.default_url = 'http://121.43.187.220/'; // http://121.43.187.220/ /api/

axios.defaults.headers['X-Requested-With'] = "XMLHttpRequest"; // 设置请求头
axios.defaults.baseURL = Vue.prototype.default_url;

// 挂载到vue原型上
Vue.prototype.axios = axios;
import {DatePicker, Select, Option, Button, Table, TableColumn, Pagination, Input, MessageBox, Message,Checkbox} from 'element-ui';
import {Loading, Row, Col, Collapse, CollapseItem, Dialog} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// 初始化css
import '../static/css/base.css'
// 使用elementUI
Vue.use(DatePicker);
Vue.use(Select);
Vue.use(Option);
Vue.use(Button);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Input);
Vue.use(Checkbox);
Vue.use(Loading);
Vue.use(Row);
Vue.use(Col);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Dialog);
Vue.config.productionTip = false;

Vue.prototype.ElementUIMsg = Message;
Vue.prototype.ElementUIMsgBox = MessageBox;

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  Vue.prototype.$isDaifu = false;
  if (to.path.match(/payroll/)) { // 代付
    Vue.prototype.$isDaifu = true;
  }
  if (to.path !== '/login' && !to.path.match(/404/)) {
    if (!localStorage['token']) {
      router.replace({
        path: '/login'
      });
      return false;
    }
    next();
    return false;
  }
  next();
  return false;
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
